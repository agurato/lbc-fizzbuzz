FROM arm64v8/golang:1.18

WORKDIR /usr/src/app

# pre-copy/cache go.mod for pre-downloading dependencies and only redownloading them in subsequent builds if they change
COPY go.mod go.sum ./
RUN go mod download && go mod verify

COPY cmd/ ./cmd
COPY internal/ ./internal
RUN go build -o /usr/local/bin/app ./cmd/fizzbuzz/ 

CMD ["app"]