Fizzbuzz server for LeBonCoin
===

## Test the server

The server is currently running in a docker container on my raspberry pi at home, and available at [https://fizzbuzz.vmonot.dev/](https://fizzbuzz.vmonot.dev/).

It exposes two endpoints:
- `GET /fizzbuzz` ([https://fizzbuzz.vmonot.dev/fizzbuzz](https://fizzbuzz.vmonot.dev/fizzbuzz)):
    - Accepts 5 query parameters: `int1`, `int2`, `limit`, `str1`, and `str2`
    - Outputs a fizzbuzz
- `GET /stats` ([https://fizzbuzz.vmonot.dev/stats](https://fizzbuzz.vmonot.dev/stats)): 
    - Display the request with the highest number of calls

## Technical decisions

### Projetc architecture

I'm following [this project layout](https://github.com/golang-standards/project-layout) in professional and personal projects as it allows the source code to be properly separated and organized.

### Endpoints

All endpoints return a **HTTP Status Ok (200)** and a **JSON response**, whether the request has succeeded or not.

If the request has failed (eg no access to database for the stats endpoint or wrong input parameter for the fizzbuzz endpoint), the JSON will contain an error message. If the request succeeded, the JSON will contain the expected result.

In case of errors, I could have returned a HTTP Status 400 (for generic errors) or 422 (for bad input parameters) instead of a 200, but I didn't want to mix the HTTP layer (connection between client and server) with the application layer (this server). If this was a bigger project with a highest number of possible errors, the JSON would also return an application error code corresponding to each error.

### Web server

I decided to use the **Gin** library, because I'm familiar with it and didn't want to "re-invent the wheel" by coding a server from scratch. It has many features and adding features to the server would be very easy using this library.

### Database

I chose **MongoDB** to save requests, because I've recently used it in a couple of personal projects, and I've really liked it so far, as it's much easier to use than a SQL database.

I decided to not have a document for each request, but rather to have a document corresponding to a group of 5 parameters (`int1`, `int2`, `limit`, `str1`, and `str2`), alongside a count variable that is incremented each time this group of 5 parameters is called. This allows for a reduced disk space used, as having a full history of requests is not needed.

## Run the server yourself

### Run from Docker container (arm64v8 only)

```yml
# docker-compose.yml

version: '3.3'

services:

  # Mongo database
  mongo:
    container_name: mongo
    image: mongo
    ports:
      - 27017:27017
    environment:
      MONGO_INITDB_ROOT_USERNAME: username
      MONGO_INITDB_ROOT_PASSWORD: password
    volumes:
      - mongo-data:/data/db
    restart: unless-stopped

  # Fizzbuzz server
  fizzbuzz:
    container_name: fizzbuzz
    image: agurato/lbc-fizzbuzz
    ports:
      - 8080:8080
    environment:
      DB_URL: mongo
      DB_PORT: 27017
      DB_NAME: lbc-fizzbuzz
      DB_USER: username
      DB_PASSWORD: password
    depends_on:
      - mongo

volumes:
  mongo-data:
```

### Compile and build locally

1. Create `.env` file at the root of the project and fill the MongoDB connection information.
```env
# .env

DB_URL=
DB_PORT=
DB_NAME=
DB_USER=
DB_PASSWORD=
```

2. Build project and run it. Access the endpoints via [http://localhost:8080/](http://localhost:8080/)
```sh
go mod download
go mod verify
go build ./cmd/fizzbuzz
./fizzbuzz
```

3. *Optional*: Build docker container for **arm64v8** and run it. Access the endpoints via [http://localhost:8080/](http://localhost:8080/).
```sh
docker build -t lbc-fizzbuzz:latest .
docker run --rm --name fizzbuzz --env-file ./.env -p 8080:8080 lbc-fizzbuzz
```