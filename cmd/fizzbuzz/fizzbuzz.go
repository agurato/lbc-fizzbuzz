package main

import (
	"github.com/joho/godotenv"
	fizzbuzz "gitlab.com/agurato/lbc-fizzbuzz/internal"
)

func main() {
	godotenv.Load()
	fizzbuzz.InitDB()
	fizzbuzz.Server.Run()
}
