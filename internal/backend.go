package fizzbuzz

import (
	"context"
	"errors"
	"fmt"
	"os"
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// RequestNumber holds parameters with the number of times it has been called
type RequestNumber struct {
	Int1  int    `bson:"int1"`
	Int2  int    `bson:"int2"`
	Limit int    `bson:"limit"`
	Str1  string `bson:"str1"`
	Str2  string `bson:"str2"`
	Count uint   `bson:"count,omitempty"`
}

var (
	MongoCtx       context.Context
	mongoDb        *mongo.Database
	mongoRequestNb *mongo.Collection
)

// Initializes database
func InitDB() {
	mongoCtx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	mongoClient, err := mongo.Connect(mongoCtx,
		options.Client().ApplyURI(fmt.Sprintf("mongodb://%s:%s@%s:%s",
			os.Getenv("DB_USER"),
			os.Getenv("DB_PASSWORD"),
			os.Getenv("DB_URL"),
			os.Getenv("DB_PORT"))))
	if err != nil {
		log.WithField("error", err).Fatal("Could not connect to database")
		return
	}
	mongoDb = mongoClient.Database(os.Getenv("DB_NAME"))
	mongoRequestNb = mongoDb.Collection("requestNb")
}

// GetFizzBuzz returns a list of strings with numbers from 1 to limit, where:
// all multiples of int1 are replaced by str1,
// all multiples of int2 are replaced by str2,
// all multiples of int1 and int2 are replaced by str1str2
func GetFizzBuzz(int1, int2, limit int, str1, str2 string) (ret []string) {
	for i := 1; i <= limit; i++ {
		var (
			tmpValue string
			added    bool
		)
		// If multiple of int1, set to str1
		if i%int1 == 0 {
			tmpValue = str1
			added = true
		}
		// If multiple of int2, concatenate with str2
		if i%int2 == 0 {
			tmpValue += str2
			added = true
		}
		// Else, add number in base 10
		if !added {
			tmpValue = strconv.Itoa(i)
		}
		ret = append(ret, tmpValue)
	}

	return
}

// AddToStats adds the parameters to the MongoDB database
// If these parameters were already in DB, it increases its count instead
func AddToStats(int1, int2, limit int, str1, str2 string) {
	// Filter for the update operation
	filter := RequestNumber{Int1: int1, Int2: int2, Limit: limit, Str1: str1, Str2: str2}
	// Update operation (increase count by 1)
	update := bson.D{{Key: "$inc", Value: bson.D{{Key: "count", Value: 1}}}}
	// Upsert (insert if non existent) instead of normal update
	opts := options.Update().SetUpsert(true)

	// Commit operation to the database
	result, err := mongoRequestNb.UpdateOne(MongoCtx, filter, update, opts)
	if err != nil {
		log.WithField("error", err).Error("Could not update statistics in database")
		return
	}
	if result.ModifiedCount == 0 && result.UpsertedCount == 0 {
		log.Warning("Statistics were not updated in database")
	} else {
		log.Debug("Statistics were updated in database")
	}
}

// GetMostCalledRequest returns the request parameters with the highest number of calls
func GetMostCalledRequest() (mostCalledRequest RequestNumber, err error) {
	err = nil

	if mongoRequestNb == nil {
		err = fmt.Errorf("Database is not connected")
		return
	}

	// Sort descending by count. Optimised when used with limit
	findOpts := options.FindOne().SetSort(bson.D{{Key: "count", Value: -1}})

	// Get first result (highest count)
	result := mongoRequestNb.FindOne(MongoCtx, bson.M{}, findOpts)
	if result.Err() != nil {
		err = errors.New("No request yet")
		return
	}

	// Decode into struct
	err = result.Decode(&mostCalledRequest)
	if err != nil {
		err = errors.New("Failed to fetch request from database")
		return
	}

	return
}
