package fizzbuzz_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	fizzbuzz "gitlab.com/agurato/lbc-fizzbuzz/internal"
)

func TestGetFizzBuzz(t *testing.T) {
	ret := fizzbuzz.GetFizzBuzz(3, 5, 16, "fizz", "buzz")
	fmt.Println(ret)
	assert.Equal(t, []string{"1", "2", "fizz", "4", "buzz", "fizz", "7", "8", "fizz", "buzz", "11", "fizz", "13", "14", "fizzbuzz", "16"}, ret)
}
