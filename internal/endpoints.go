package fizzbuzz

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

var (
	Server *gin.Engine
)

func init() {
	gin.SetMode(gin.ReleaseMode)
	Server = gin.Default()
	Server.SetTrustedProxies(nil)

	Server.GET("/fizzbuzz", HandleGETFizzbuzz)
	Server.GET("/stats", HandleGETStats)
}

// GET endpoint returns JSON containing either the computed fizzbuzz or an error message.
//
// Returns:
//  {
//    "error": <error message>,
//    "result": [<list of fizzbuzz elements>],
//  }
func HandleGETFizzbuzz(c *gin.Context) {
	var (
		err               error
		int1, int2, limit int
		str1, str2        string
	)

	// Fetch all parameters
	// All errors are returns with a HTTP status OK (200), but with an error message in the JSON
	if int1, err = getIntInput(c, "int1"); err != nil {
		c.JSON(http.StatusOK, gin.H{"error": err.Error()})
		return
	}
	if int2, err = getIntInput(c, "int2"); err != nil {
		c.JSON(http.StatusOK, gin.H{"error": err.Error()})
		return
	}
	if limit, err = getIntInput(c, "limit"); err != nil {
		c.JSON(http.StatusOK, gin.H{"error": err.Error()})
		return
	}
	if str1, err = getStringInput(c, "str1"); err != nil {
		c.JSON(http.StatusOK, gin.H{"error": err.Error()})
		return
	}
	if str2, err = getStringInput(c, "str2"); err != nil {
		c.JSON(http.StatusOK, gin.H{"error": err.Error()})
		return
	}

	// Check that limit >= 1
	if limit < 1 {
		c.JSON(http.StatusOK, gin.H{"error": "limit must be >= 1"})
		return
	}

	// Return fizzbuzz result
	c.JSON(http.StatusOK, gin.H{"result": GetFizzBuzz(int1, int2, limit, str1, str2)})

	// Add to stats from another coroutine to reply to client ASAP
	go AddToStats(int1, int2, limit, str1, str2)
}

// HandleGETStats endpoint returns JSON containing either the most called parameters with the number of calls, or an error message.
//
// Returns:
//  {
//    "error": "<error message>",
//    "result": {
//      "calls": <number of calls>,
//      "parameters": {<dict of input parameters>},
//    },
//  }
func HandleGETStats(c *gin.Context) {
	// Get request with highest number of calls
	mostCalledRequest, err := GetMostCalledRequest()
	if err != nil {
		c.JSON(http.StatusOK, gin.H{"error": err.Error()})
		return
	}
	// Return result
	c.JSON(http.StatusOK, gin.H{
		"result": gin.H{
			"calls": mostCalledRequest.Count,
			"parameters": gin.H{
				"int1":  mostCalledRequest.Int1,
				"int2":  mostCalledRequest.Int2,
				"limit": mostCalledRequest.Limit,
				"str1":  mostCalledRequest.Str1,
				"str2":  mostCalledRequest.Str2,
			},
		}})
}
