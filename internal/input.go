package fizzbuzz

import (
	"fmt"
	"strconv"

	"github.com/gin-gonic/gin"
)

func getStringInput(c *gin.Context, query string) (string, error) {
	var (
		ok          bool
		inputString string
	)
	if inputString, ok = c.GetQuery(query); !ok {
		return "", fmt.Errorf("Missing %s parameter", query)
	}
	return inputString, nil
}

func getIntInput(c *gin.Context, query string) (int, error) {
	var (
		err         error
		inputString string
		inputInt    int
	)
	if inputString, err = getStringInput(c, query); err != nil {
		return 0, err
	}
	if inputInt, err = strconv.Atoi(inputString); err != nil {
		return 0, fmt.Errorf("%s should be an integer", query)
	}
	return inputInt, nil
}
